import argparse
from director import *

def main():
    print("puffin: instantaneous python")

    # Set up argument parser
    parser = argparse.ArgumentParser(description="Puffin Compiler")
    parser.add_argument('-s', '--source', help='Source file')
    parser.add_argument('-o', '--output', help='Output file')
    parser.add_argument('-v', '--verbose', action='store_true', help='Verbose mode')

    # Parse command line arguments
    pargs = parser.parse_args()
    args = Args(source=pargs.source, output=pargs.output, verbose=pargs.verbose)

    # Instantiate Director and call start()
    director = Director(args)
    director.start()

if __name__ == "__main__":
    main()
