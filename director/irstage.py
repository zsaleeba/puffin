"""
An IR Stage is an output of one stage (or 'pass') in the compilation 
process and an input to the next stage in the compilation process. It
identifies both what is being compiled and where in the compilation 
process it is up to.

It doesn't define the data structures, just names what item it relates 
to and what stage it's up to.

A StepName names a stage in the compilation process. In other words, 
it names which pass of the compiler produced the output.

A path is a unique path of the compilation unit being processed. This
is used to identify which compilation unit the job is associated with.

eg.
    somedir/somefile.py                      - a source file
    somedir/somefile.py/somefunc             - a function in a source file
    somedir/somefile.py/SomeClass.someMethod - a method in a class in a source file

When a source function or source file is deleted, the path identifies 
which IR stages need to be deleted.
"""
import enum


class StepName(enum.Enum):
    SOURCE_FILE = 1              # A source file
    TOKEN_SET = 2                # The tokens produced by the lexer for a single file
    CST = 3                      # The concrete syntax tree produced by the parser for a single function or top level declaration
    AST = 4                      # The abstract syntax tree produced by the semantic analyzer for a single function or top level declaration
    BBA_CFG = 5                  # The basic block analysis output - a control flow graph for a single function
    RENAMED_CFG = 6              # The variable-renamed control flow graph for a single function
    PHI_INSERTED_CFG = 7         # The phi function inserted control flow graph for a single function
    SSA_CFG = 8                  # The static single assignment control flow graph for a single function
    CONST_PROPAGATED_CFG = 9     # The constant propagated control flow graph for a single function
    DEAD_CODE_ELIM_CFG = 10      # The dead code eliminated control flow graph for a single function
    COPY_PROPAGATED_CFG = 11     # The copy propagated control flow graph for a single function
    CSE_CFG = 12                 # The common subexpression eliminated control flow graph for a single function
    LOOP_OPT_CFG = 13            # The loop optimized control flow graph for a single function
    STRENGTH_REDUCED_CFG = 14    # The strength reduced control flow graph for a single function
    SPARSE_COND_FOLDED_CFG = 15  # The sparse conditional folded control flow graph for a single function
    INLINED_CFG = 16             # The inlined control flow graph for a single function
    TAIL_REC_OPT_CFG = 17        # The tail recursion optimized control flow graph for a single function
    SSA_ELIM_CFG = 18            # The static single assignment eliminated control flow graph for a single function
    INSTR_SELECTION_CFG = 19     # The instruction selected control flow graph for a single function
    REG_ALLOCATED_CFG = 20       # The register allocated control flow graph for a single function
    INSTR_SCHED_CFG = 21         # The instruction scheduled control flow graph for a single function
    PEEPHOLE_OPT_CFG = 22        # The peephole optimized control flow graph for a single function
    OBJECT_CODE = 23             # The object code for a single function
    LINKED_OBJECT_CODE = 24      # The linked object code for the entire program


class IrStage:
    # A unique path of the compilation unit being processed. This is used to 
    #   identify which compilation unit the job is associated with.
    #   eg. somedir/somefile.py or somedir/somefile.py/somefunc
    def __init__(self, step=StepName.SOURCE_FILE, path=None):
        self.step = step
        self.path = path

    def __str__(self):
        return str(self.path) + ": " + str(self.step)

    def __repr__(self):
        return str(self.path) + ": " + str(self.step)
