"""
The Job class encapsulates a compilation job.

A multiprocessing approach is used to parallelize the compilation process.
A queue of jobs to be executed is kept and a pool of workers to execute them.
For each job it tracks the job's dependencies and the jobs that depend on it.

An example of a job would be "perform basic block analysis on function foo". 
It might be dependent on "ast produced for function foo" and "define global 
variable bar". In turn it might produce "control flow graph for function foo".

Each job has:
    * A set of input dependencies. This is a list of IrStages that the job 
      depends on.
    * A set of output products. This is a list of IrStages that the job 
      produces.
    * Code to run to go from the input dependencies to the output products.
"""
from irstage import IrStage

class Job:
    def __init__(self, inputs=None, outputs=None, code=None):
        # Initialize the job
        self.inputs = inputs
        self.outputs = outputs
        self.code = code

