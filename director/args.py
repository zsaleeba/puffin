"""
The Args class encapsulates the command line arguments of the compiler.
"""
class Args:
    def __init__(self, source=None, output=None, verbose=False):
        # Initialize the arguments
        self.source = source
        self.output = output
        self.verbose = verbose

    def setSource(self, source):
        # Set the source file
        self.source = source

    def setOutput(self, output):
        # Set the output file
        self.output = output

    def setVerbose(self, verbose):
        # Set the verbose mode
        self.verbose = verbose
