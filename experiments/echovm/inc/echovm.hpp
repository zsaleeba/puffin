#pragma once

#include <memory>
#include <functional>
#include <cstdint>

#include "jsonrpccxx/server.hpp"
#include "cpphttplibconnector.hpp"


class EchoVm
{
public:
    static constexpr int ECHOVM_PORT = 14325;

public:
    EchoVm();
    ~EchoVm();

    // Run the runtime system
    void run();

    // Code update commands
    void cmdClearCode();
    void cmdSetCode(const std::string &entityName, const std::string& code);

    // Runtime shuttle control commands
    void cmdPlayForward();
    void cmdPlayReverse();
    void cmdSingleStepForward();
    void cmdSingleStepReverse();
    void cmdSkipTo(uint64_t pos);

private:
    // Initialise the RPC server
    void initRpcServer();

private:
    // JSON RPC server for commands from clients
    std::shared_ptr<jsonrpccxx::JsonRpc2Server> rpcServer;
    std::shared_ptr<CppHttpLibServerConnector>  httpServer;
};
