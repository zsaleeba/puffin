#include <iostream>
#include <memory>
#include <functional>

#include "echovm.hpp"
#include "jsonrpccxx/server.hpp"
#include "cpphttplibconnector.hpp"

using namespace std::placeholders;
using namespace jsonrpccxx;


EchoVm::EchoVm()
{
    // Set up the RPC server
    initRpcServer();
}

EchoVm::~EchoVm()
{
}

void EchoVm::initRpcServer()
{
    // Create the JSON RPC server
    rpcServer = std::make_shared<JsonRpc2Server>();

    // Set up the RPC commands
    rpcServer->Add("cmdClearCode", GetHandle(&EchoVm::cmdClearCode, *this), {});
    rpcServer->Add("cmdSetCode", GetHandle(&EchoVm::cmdSetCode, *this), {{"entityName", "string"}, {"code", "string"}});
    rpcServer->Add("cmdPlayForward", GetHandle(&EchoVm::cmdPlayForward, *this), {});
    rpcServer->Add("cmdPlayReverse", GetHandle(&EchoVm::cmdPlayReverse, *this), {});
    rpcServer->Add("cmdSingleStepForward", GetHandle(&EchoVm::cmdSingleStepForward, *this), {});
    rpcServer->Add("cmdSingleStepReverse", GetHandle(&EchoVm::cmdSingleStepReverse, *this), {});
    rpcServer->Add("cmdSkipTo", GetHandle(&EchoVm::cmdSkipTo, *this), {{"pos", "uint64"}});

    // Create the HTTP server connector
    httpServer = std::make_shared<CppHttpLibServerConnector>(*rpcServer, ECHOVM_PORT);
}

void EchoVm::run()
{
    // Wait indefinitely
    while (true) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
}

void EchoVm::cmdClearCode()
{
    std::cout << "EchoVm::cmdClearCode()" << std::endl;
}

void EchoVm::cmdSetCode(const std::string &entityName, const std::string& code)
{
    std::cout << "EchoVm::cmdSetCode(" << entityName << ", " << code << ")" << std::endl;
}

void EchoVm::cmdPlayForward()
{
    std::cout << "EchoVm::cmdPlayForward()" << std::endl;
}

void EchoVm::cmdPlayReverse()
{
    std::cout << "EchoVm::cmdPlayReverse()" << std::endl;
}

void EchoVm::cmdSingleStepForward()
{
    std::cout << "EchoVm::cmdSingleStepForward()" << std::endl;
}

void EchoVm::cmdSingleStepReverse()
{
    std::cout << "EchoVm::cmdSingleStepReverse()" << std::endl;
}

void EchoVm::cmdSkipTo(uint64_t pos)
{
    std::cout << "EchoVm::cmdSkipTo(" << pos << ")" << std::endl;
}

