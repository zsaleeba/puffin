//
// echovm interactive runtime system
//

#include <iostream>
#include "echovm.hpp"

#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)


int main()
{
    std::cout << "echovm - echopython runtime v" << TOSTRING(VERSION) << std::endl;
    EchoVm vm;
    vm.run();

    // Wait indefinitely
    while (true) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

    return 0;
}
