import ast
from cfg_node import CfgNode

"""
Converts an AST into a CFG.
"""

class CfgConverter(ast.NodeVisitor):
    def __init__(self, ast_root):
        """Initialize the converter with the root of the AST."""
        self.ast_root = ast_root
        self.current_node = CfgNode()
        self.cfg_nodes = [self.current_node]
        self.loop_stack = []  # Stack to keep track of loops for break/continue statements


    def visit(self, node):
        """Visit a node and process it."""
        match type(node):
            case ast.FunctionDef:
                return self.visit_FunctionDef(node)
            case ast.If:
                return self.visit_If(node)
            case ast.IfExp:
                return self.visit_IfExp(node)
            case ast.For:
                return self.visit_For(node)
            case ast.While:
                return self.visit_While(node)
            case ast.Try:
                return self.visit_Try(node)
            case ast.With:
                return self.visit_With(node)
            case ast.Yield:
                return self.visit_Yield(node)
            case ast.YieldFrom:
                return self.visit_YieldFrom(node)
            case ast.Raise:
                return self.visit_Raise(node)
            case ast.Assert:
                return self.visit_Assert(node)
            case ast.Import:
                return self.visit_Import(node)
            case ast.ImportFrom:
                return self.visit_ImportFrom(node)
            case ast.Global:
                return self.visit_Global(node)
            case ast.Nonlocal:
                return self.visit_Nonlocal(node)
            case ast.Assign:
                return self.visit_Assign(node)
            case ast.AugAssign:
                return self.visit_AugAssign(node)
            case ast.AnnAssign:
                return self.visit_AnnAssign(node)
            case ast.Return:
                return self.visit_Return(node)
            case ast.Break:
                return self.visit_Break(node)
            case ast.Continue:
                return self.visit_Continue(node)
            case ast.Lambda:
                return self.visit_Lambda(node)
            case ast.Expr:
                return self.visit_Expr(node)
            case ast.Call:
                return self.visit_Call(node)
            case ast.Name:
                return self.visit_Name(node)
            case ast.Constant:
                return self.visit_Constant(node)
            case _:
                return self.generic_visit(node)
            

    def generic_visit(self, node):
        """Called if no explicit visitor function exists for a node."""
        for field, value in ast.iter_fields(node):
            if isinstance(value, list):
                for item in value:
                    if isinstance(item, ast.AST):
                        self.visit(item)
            elif isinstance(value, ast.AST):
                self.visit(value)


    def visit_FunctionDef(self, node):
        """Process a function definition."""
        new_node = CfgNode()
        self.current_node.add_successor(new_node)
        new_node.add_predecessor(self.current_node)
        self.current_node = new_node
        self.cfg_nodes.append(new_node)
        self.generic_visit(node)

    
    def visit_If(self, node):
        """Process an if statement."""
        if_node = CfgNode()
        if_node.is_branch = True
        self.current_node.add_successor(if_node)
        if_node.add_predecessor(self.current_node)

        # True branch
        self.current_node = if_node
        self.visit(node.test)
        true_node = CfgNode()
        self.current_node.add_successor(true_node)
        true_node.add_predecessor(self.current_node)
        self.current_node = true_node
        self.cfg_nodes.append(true_node)
        for n in node.body:
            self.visit(n)

        # False branch
        false_node = CfgNode()
        if_node.add_successor(false_node)
        false_node.add_predecessor(if_node)
        self.current_node = false_node
        self.cfg_nodes.append(false_node)
        for n in node.orelse:
            self.visit(n)

        # Merge node after the branches
        merge_node = CfgNode()
        true_node.add_successor(merge_node)
        false_node.add_successor(merge_node)
        merge_node.add_predecessor(true_node)
        merge_node.add_predecessor(false_node)
        self.current_node = merge_node
        self.cfg_nodes.append(merge_node)


    def visit_IfExp(self, node):
        """Process an if expression."""
        # Node for the condition
        condition_node = CfgNode()
        self.current_node.add_successor(condition_node)
        condition_node.add_predecessor(self.current_node)
        self.current_node = condition_node
        self.cfg_nodes.append(condition_node)

        # Evaluate the condition
        self.visit(node.test)

        # Node for the 'true' branch
        true_node = CfgNode()
        condition_node.add_successor(true_node)
        true_node.add_predecessor(condition_node)
        self.current_node = true_node
        self.cfg_nodes.append(true_node)
        self.visit(node.body)  # The expression if the condition is true

        # Node for the 'false' branch
        false_node = CfgNode()
        condition_node.add_successor(false_node)
        false_node.add_predecessor(condition_node)
        self.current_node = false_node
        self.cfg_nodes.append(false_node)
        self.visit(node.orelse)  # The expression if the condition is false

        # Merge node after the branches
        merge_node = CfgNode()
        true_node.add_successor(merge_node)
        false_node.add_successor(merge_node)
        merge_node.add_predecessor(true_node)
        merge_node.add_predecessor(false_node)
        self.current_node = merge_node
        self.cfg_nodes.append(merge_node)
        
    
    def visit_While(self, node):
        """Start of the 'while' loop."""
        while_node = CfgNode()
        while_node.is_loop_header = True
        self.current_node.add_successor(while_node)
        while_node.add_predecessor(self.current_node)

        # Save the current node and while node for break and continue
        previous_node = self.current_node
        self.current_node = while_node
        self.cfg_nodes.append(while_node)
        self.loop_stack.append((while_node, CfgNode()))  # (loop start, loop end)

        # Loop condition
        self.visit(node.test)

        # Loop body
        body_node = CfgNode()
        self.current_node.add_successor(body_node)
        body_node.add_predecessor(self.current_node)
        self.current_node = body_node
        self.cfg_nodes.append(body_node)

        for n in node.body:
            self.visit(n)

        # Connect back to the while_node for the next iteration
        body_node.add_successor(while_node)

        # After loop node (end of the loop)
        after_loop_node = self.loop_stack[-1][1]
        while_node.add_successor(after_loop_node)
        after_loop_node.add_predecessor(while_node)

        # Restore the current node and pop the loop from the stack
        self.current_node = after_loop_node
        self.cfg_nodes.append(after_loop_node)
        self.loop_stack.pop()


    def visit_For(self, node):
        """Start of the 'for' loop."""
        for_node = CfgNode()
        for_node.is_loop_header = True
        self.current_node.add_successor(for_node)
        for_node.add_predecessor(self.current_node)

        # Save the current node and for node for break and continue
        previous_node = self.current_node
        self.current_node = for_node
        self.cfg_nodes.append(for_node)
        self.loop_stack.append((for_node, CfgNode()))  # (loop start, loop end)

        # Visit the 'for' loop iterator and target
        self.visit(node.target)
        self.visit(node.iter)

        # Loop body
        body_node = CfgNode()
        self.current_node.add_successor(body_node)
        body_node.add_predecessor(self.current_node)
        self.current_node = body_node
        self.cfg_nodes.append(body_node)

        for n in node.body:
            self.visit(n)

        # Connect back to the for_node for the next iteration
        body_node.add_successor(for_node)

        # After loop node (end of the loop)
        after_loop_node = self.loop_stack[-1][1]
        for_node.add_successor(after_loop_node)
        after_loop_node.add_predecessor(for_node)

        # Visit the 'else' part of the 'for' loop if it exists
        self.current_node = after_loop_node
        for n in node.orelse:
            self.visit(n)

        # Set the current node to after_loop_node
        self.current_node = after_loop_node
        self.cfg_nodes.append(after_loop_node)
        self.loop_stack.pop()


    def visit_Return(self, node):
        """Process a return statement."""
        return_node = CfgNode()
        self.current_node.add_successor(return_node)
        return_node.add_predecessor(self.current_node)
        return_node.add_statement(node)
        self.cfg_nodes.append(return_node)
        self.current_node = return_node    


    def visit_Break(self, node):
        """Break statement jumps to the end of the nearest enclosing loop."""
        if self.loop_stack:
            loop_start, loop_end = self.loop_stack[-1]
            break_node = CfgNode()
            self.current_node.add_successor(break_node)
            break_node.add_predecessor(self.current_node)
            break_node.add_successor(loop_end)
            loop_end.add_predecessor(break_node)
            self.cfg_nodes.append(break_node)
            self.current_node = CfgNode()  # Create a new node for statements after break


    def visit_Continue(self, node):
        """Continue statement jumps to the start of the nearest enclosing loop."""
        if self.loop_stack:
            loop_start, _ = self.loop_stack[-1]
            continue_node = CfgNode()
            self.current_node.add_successor(continue_node)
            continue_node.add_predecessor(self.current_node)
            continue_node.add_successor(loop_start)
            loop_start.add_predecessor(continue_node)
            self.cfg_nodes.append(continue_node)
            self.current_node = CfgNode()  # Create a new node for statements after continue
            

    def visit_Lambda(self, node):
        """Process a lambda expression."""
        # Lambda expressions are self-contained, so we add them to the current node
        lambda_node = CfgNode()
        lambda_node.add_statement(node)
        self.current_node.add_successor(lambda_node)
        lambda_node.add_predecessor(self.current_node)
        
        # Since lambda is an expression, it does not change the control flow.
        # Thus, we don't need to change the current node.
        self.cfg_nodes.append(lambda_node)

        # Visit the body of the lambda
        self.visit(node.body)
        
        
    def visit_Assign(self, node):
        """Standard assignment (e.g., x = y)."""
        assign_node = CfgNode()
        assign_node.add_statement(node)
        self.current_node.add_successor(assign_node)
        assign_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(assign_node)
        self.current_node = assign_node

        # Visit the value being assigned
        self.visit(node.value)


    def visit_AugAssign(self, node):
        """Augmented assignment (e.g., x += y)."""
        aug_assign_node = CfgNode()
        aug_assign_node.add_statement(node)
        self.current_node.add_successor(aug_assign_node)
        aug_assign_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(aug_assign_node)
        self.current_node = aug_assign_node

        # Visit the value being assigned
        self.visit(node.value)


    def visit_AnnAssign(self, node):
        """Annotated assignment (e.g., x: int = y)."""
        ann_assign_node = CfgNode()
        ann_assign_node.add_statement(node)
        self.current_node.add_successor(ann_assign_node)
        ann_assign_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(ann_assign_node)
        self.current_node = ann_assign_node

        # Visit the value being assigned, if any
        if node.value is not None:
            self.visit(node.value)
            
            
    def visit_With(self, node):
        """Process a 'with' statement."""
        # Start of the 'with' statement
        with_node = CfgNode()
        self.current_node.add_successor(with_node)
        with_node.add_predecessor(self.current_node)
        self.current_node = with_node
        self.cfg_nodes.append(with_node)

        # Visit 'with' context expressions
        for item in node.items:
            self.visit(item.context_expr)
            if item.optional_vars:
                self.visit(item.optional_vars)

        # Visit the body of the 'with' block
        body_node = CfgNode()
        self.current_node.add_successor(body_node)
        body_node.add_predecessor(self.current_node)
        self.current_node = body_node
        self.cfg_nodes.append(body_node)

        for n in node.body:
            self.visit(n)

        # Create a new node for after the 'with' block
        after_with_node = CfgNode()
        body_node.add_successor(after_with_node)
        after_with_node.add_predecessor(body_node)
        self.current_node = after_with_node
        self.cfg_nodes.append(after_with_node)


    def visit_Yield(self, node):
        """Handle a yield statement."""
        yield_node = CfgNode()
        yield_node.add_statement(node)
        self.current_node.add_successor(yield_node)
        yield_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(yield_node)

        # The current node is updated to the new yield node
        # as it represents a potential pause and resume point in the control flow.
        self.current_node = yield_node

        # Visit the value being yielded
        if node.value is not None:
            self.visit(node.value)


    def visit_YieldFrom(self, node):
        """Handle a yield from statement (used for yielding from an iterator or generator)."""
        yield_from_node = CfgNode()
        yield_from_node.add_statement(node)
        self.current_node.add_successor(yield_from_node)
        yield_from_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(yield_from_node)

        # Update the current node to the new yield from node
        self.current_node = yield_from_node

        # Visit the value being yielded from
        self.visit(node.value)


    def visit_Import(self, node):
        """Handle an import statement (e.g., import module)."""
        import_node = CfgNode()
        import_node.add_statement(node)
        self.current_node.add_successor(import_node)
        import_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(import_node)

        # The current node is updated to the new import node.
        self.current_node = import_node


    def visit_ImportFrom(self, node):
        """Handle an import-from statement (e.g., from module import name)."""
        import_from_node = CfgNode()
        import_from_node.add_statement(node)
        self.current_node.add_successor(import_from_node)
        import_from_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(import_from_node)

        # Update the current node to the new import-from node.
        self.current_node = import_from_node


    def visit_Global(self, node):
        """Handle a global statement (e.g., global x)."""
        global_node = CfgNode()
        global_node.add_statement(node)
        self.current_node.add_successor(global_node)
        global_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(global_node)

        # The current node is updated to the new global node.
        self.current_node = global_node


    def visit_Nonlocal(self, node):
        """Handle a nonlocal statement (e.g., nonlocal x)."""
        nonlocal_node = CfgNode()
        nonlocal_node.add_statement(node)
        self.current_node.add_successor(nonlocal_node)
        nonlocal_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(nonlocal_node)

        # Update the current node to the new nonlocal node.
        self.current_node = nonlocal_node


    def visit_Call(self, node):
        """Handle a function/method call (e.g., foo(x, y))."""
        call_node = CfgNode()
        call_node.add_statement(node)
        self.current_node.add_successor(call_node)
        call_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(call_node)

        # Update the current node to the new call node.
        self.current_node = call_node

        # Visit the function being called and its arguments
        self.visit(node.func)
        for arg in node.args:
            self.visit(arg)
        for keyword in node.keywords:
            self.visit(keyword.value)


    def visit_Raise(self, node):
        """Handle a raise statement (e.g., raise Exception("Error"))."""
        raise_node = CfgNode()
        raise_node.add_statement(node)
        self.current_node.add_successor(raise_node)
        raise_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(raise_node)

        # Update the current node to the new raise node.
        self.current_node = raise_node

        # Visit the exception being raised, if any
        if node.exc is not None:
            self.visit(node.exc)
        if node.cause is not None:
            self.visit(node.cause)


    def visit_Assert(self, node):
        """Handle an assert statement (e.g., assert x > 0, "x must be positive")."""
        assert_node = CfgNode()
        assert_node.add_statement(node)
        self.current_node.add_successor(assert_node)
        assert_node.add_predecessor(self.current_node)
        self.cfg_nodes.append(assert_node)

        # Update the current node to the new assert node.
        self.current_node = assert_node

        # Visit the test and the error message in the assert statement
        self.visit(node.test)
        if node.msg is not None:
            self.visit(node.msg)


    def build_cfg(self):
        """Build the CFG."""
        self.visit(self.ast_root)
        return self.cfg_nodes


    def print_cfg(self):
        """Prints out the CFG."""
        for node in self.cfg_nodes:
            node.print_details()
