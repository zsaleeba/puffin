"""
This file contains the definition of a CFGNode class, which represents a node in a control flow graph.
"""
class CfgNode:
    def __init__(self):
        self.statements = []      # List of AST nodes representing statements in this block
        self.predecessors = []    # List of CfgNodes that lead into this node
        self.successors = []      # List of CfgNodes that this node leads to
        self.is_loop_header = False  # Flag to indicate if this node is the header of a loop
        self.is_loop_exit = False    # Flag to indicate if this node is the exit of a loop
        self.is_branch = False       # Flag to indicate if this node represents a branch
        # Additional properties can be added as needed

    def add_predecessor(self, pred_node):
        """Add a predecessor to the node."""
        if pred_node not in self.predecessors:
            self.predecessors.append(pred_node)

    def add_successor(self, succ_node):
        """Add a successor to the node."""
        if succ_node not in self.successors:
            self.successors.append(succ_node)

    def add_statement(self, statement):
        """Add a statement to the node."""
        self.statements.append(statement)

    def __str__(self):
        return f"CFGNode({len(self.statements)} statements, {len(self.predecessors)} preds, {len(self.successors)} succs)"

    def print_details(self):
        """Prints out the details of this CFG node."""
        print("Node Details:")
        print("    Statements:")
        for stmt in self.statements:
            print(f"        {ast.dump(stmt)}")

        predecessor_ids = [id(pred) for pred in self.predecessors]
        successor_ids = [id(succ) for succ in self.successors]
        print("    Predecessors:", predecessor_ids)
        print("    Successors:", successor_ids)

        flags = []
        if self.is_loop_header:
            flags.append("Loop Header")
        if self.is_loop_exit:
            flags.append("Loop Exit")
        if self.is_branch:
            flags.append("Branch")

        if flags:
            print("    Flags:", ", ".join(flags))
        print()

class PhiNode:
    def __init__(self, variable):
        self.variable = variable
        self.sources = {}  # Maps predecessor nodes to their corresponding values

    def add_source(self, pred_node, value):
        self.sources[pred_node] = value
        