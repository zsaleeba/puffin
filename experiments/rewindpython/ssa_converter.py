import ast
from cfg_node import CfgNode, PhiNode
import ast


class SsaConverter:
    def __init__(self, cfg):
        self.cfg = cfg
        self.variable_counter = {}

    def convert_to_ssa(self):
        self._insert_phi_nodes()
        self._rename_variables()

class SsaConverter:
    def __init__(self, cfg):
        # The constructor takes a CFG as input
        self.cfg = cfg
        self.variable_counter = {}

    def convert_to_ssa(self):
        # This method should convert the CFG to SSA form
        self._insert_phi_nodes()
        self._rename_variables()


    def _insert_phi_nodes(self):
        # Step 1: Identify variables and blocks where they are assigned
        var_blocks = self._find_variable_assignments()

        # Step 2: For each variable, determine where to insert Phi nodes
        for var, blocks in var_blocks.items():
            for block in blocks:
                self._insert_phi_for_variable(var, block)

    def _find_variable_assignments(self):
        # Find all variables and the blocks where they are assigned
        var_blocks = {}
        for node in self._traverse_cfg():
            for statement in node.statements:
                if isinstance(statement, ast.Assign):
                    for target in statement.targets:
                        if isinstance(target, ast.Name):
                            var = target.id
                            if var not in var_blocks:
                                var_blocks[var] = set()
                            var_blocks[var].add(node)
        return var_blocks

    def _insert_phi_for_variable(self, var, block):
        # For a given variable and block, determine where to insert Phi nodes
        # This usually requires a dominance frontier analysis
        # For simplicity, we'll assume that every predecessor needs a Phi node
        for pred in block.predecessors:
            if not self._has_phi_node(pred, var):
                phi_node = PhiNode(var)
                for p in pred.predecessors:
                    phi_node.add_source(p, var)
                pred.phi_nodes.append(phi_node)

    def _has_phi_node(self, block, var):
        return any(phi_node for phi_node in getattr(block, 'phi_nodes', []) if phi_node.variable == var)

    def _traverse_cfg(self):
        # This method should return a generator that traverses all nodes in the CFG
        # Implementing a correct traversal (e.g., depth-first) is important for correct analysis
        visited = set()
        stack = [self.cfg.entry_node]  # Assuming 'entry_node' is the entry point of the CFG

        while stack:
            node = stack.pop()
            if node in visited:
                continue
            visited.add(node)
            yield node
            stack.extend(node.successors)

    def _rename_variables(self):
        # Start the renaming process from the entry node of the CFG.
        self._rename_in_node(self.cfg.entry_node, {})

    def _rename_in_node(self, node, current_names):
        # Rename variables in the node's statements.
        new_statements = []
        for statement in node.statements:
            new_statement = self._rename_ast_nodes(statement, current_names)
            new_statements.append(new_statement)
        node.statements = new_statements

        # Process successors recursively.
        for succ in node.successors:
            self._rename_in_node(succ, current_names.copy())

    def _rename_ast_nodes(self, node, current_names):
        # A recursive method to traverse and rename variables in an AST node.
        for child in ast.iter_child_nodes(node):
            if isinstance(child, ast.Name) and isinstance(node, (ast.Assign, ast.AugAssign)):
                if child.id in current_names:
                    child.id = current_names[child.id]
                elif isinstance(node, ast.Assign):
                    new_name = self._new_name(child.id)
                    current_names[child.id] = new_name
                    child.id = new_name
            else:
                self._rename_ast_nodes(child, current_names)
        return node

    def _new_name(self, var):
        # Generate a new name for a variable.
        if var not in self.variable_counter:
            self.variable_counter[var] = 0
        self.variable_counter[var] += 1
        return f"{var}_{self.variable_counter[var]}"
