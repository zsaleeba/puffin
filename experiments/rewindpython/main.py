
import ast
from ssa_converter import SSAConverter

def parse_to_ast(filename):
    with open(filename, "r") as file:
        return ast.parse(file.read(), filename=filename)

def main(filename):
    ast_tree = parse_to_ast(filename)
    converter = SSAConverter()
    ssa_form = converter.convert(ast_tree)
    # Print or process the SSA form
    print(ssa_form)

if __name__ == "__main__":
    import sys
    if len(sys.argv) != 2:
        print("Usage: python ssa_converter.py <filename>")
        sys.exit(1)

    main(sys.argv[1])
